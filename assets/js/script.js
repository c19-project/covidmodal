document.querySelector("#travButton").addEventListener("click", function() {

	if (document.querySelector("#travY").checked) {
		$("#travelModal").modal('hide');
		$("#travel2Modal").modal();
		localStorage.setItem("Traveller", "A traveller")
	} else if (document.querySelector("#travN").checked) {	
		$("#travelModal").modal('hide');
		$("#travel2Modal").modal();
		localStorage.setItem("Traveller", "Not a traveller")
	} else if ((document.querySelector("#travY").checked == false) && (document.querySelector("#travN").checked == false)) {
		alert ("Please pick Yes or No");
	}

})

document.querySelector("#trav2Button").addEventListener("click", function() {

	if (document.querySelector("#trav2Y").checked) {
		$("#travel2Modal").modal('hide');
		$("#symptomModal").modal();
		localStorage.setItem("q2", "yes")
	} else if (document.querySelector("#trav2N").checked) {	
		$("#travel2Modal").modal('hide');
		$("#symptomModal").modal();
		// window.location.href="./na.html"
		localStorage.setItem("q2", "no")
	} else if ((document.querySelector("#trav2Y").checked == false) && (document.querySelector("#trav2N").checked == false)) {
		alert ("Please pick Yes or No");
	}

})

document.querySelector("#sympButton").addEventListener("click", function() {

	if (document.querySelector("#sympY").checked) {
		window.location.href="./results.html"
		localStorage.setItem("q3", "yes")
	} else if (document.querySelector("#sympN").checked) {	
		window.location.href="./results.html"
		localStorage.setItem("q3", "no")
	} else if ((document.querySelector("#sympY").checked == false) && (document.querySelector("#sympN").checked == false)) {
		alert ("Please pick Yes or No");
	}

})

